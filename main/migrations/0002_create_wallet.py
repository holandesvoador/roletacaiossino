# Generated by Django 5.0.2 on 2024-04-17 22:52

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


def create_wallets(apps, schema_editor):
    User = apps.get_model(settings.AUTH_USER_MODEL)
    Wallet = apps.get_model('main.Wallet')
    for user in User.objects.filter(wallet__isnull=True):
        Wallet.objects.create(user=user)


def delete_wallets(apps, schema_editor):
    Wallet = apps.get_model('main.Wallet')
    Wallet.objects.filter(user__isnull=False).delete()


class Migration(migrations.Migration):

    dependencies = [
        ("main", "0001_initial"),
    ]

    operations = [
        migrations.RunPython(create_wallets, delete_wallets)
    ]
