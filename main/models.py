from django.db import models
from django.conf import settings
from django.db.models import F
from django.db.models.signals import post_save
from django.dispatch import receiver


class Wallet(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.SET_NULL,
        null=True
    )
    total_caiocoins = models.FloatField(default=0)

class Movement(models.Model):
    created_at = models.DateField(
        auto_now_add=True,
    )

    wallet = models.ForeignKey(
        Wallet,
        on_delete=models.CASCADE
    )
    
    description = models.TextField(
        
    )
    
    value = models.FloatField(

    )

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def update_stock(sender, instance, created, raw, **kwargs):
    if not created or raw:
        return
    Wallet.objects.create(user=instance)

@receiver(post_save, sender=Movement)
def update_wallet(instance, created, raw, **kwargs):
    if raw or not created:
        return

    wallet = instance.wallet
    wallet.total_caiocoins = F('total_caiocoins') + instance.value
    wallet.save()
