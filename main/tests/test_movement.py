from django.test import TestCase
from unittest.mock import patch
import json
from urllib.parse import urlparse
from django.http import Http404
from http import HTTPStatus
from main.models import Wallet, Movement
from django.shortcuts import resolve_url
from main.tests import create_user
from roleta import Dealer, Roleta


# Create your tests here.

class AnonymousWMovementViewsTest(TestCase):

    def test_my_movement(self):
        url = resolve_url('movement')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        parsed_url = urlparse(response.url)
        self.assertEqual(parsed_url.query, 'next=' + url)
        self.assertEqual(parsed_url.path,  '/accounts/login/')

    def test_post_bets(self):
        with patch.object(Roleta, 'create_default', lambda : Roleta.create_static(1)):
            login_user = create_user()
            user_wallet  = login_user.wallet
            
            user_wallet.total_caiocoins = 1000
            user_wallet.save()

            self.client.force_login(login_user)
            
            url = resolve_url('postbets')
            
            inputs =  [
                {
                'id': login_user.pk,
                'value': 100.0,
                'str': 'red'
                },
                {
                'id': login_user.pk,
                'value': 5.0,
                'str': '2'
                }
            ]

            expected_output = {
                'secret_number': 1,
                'before_bet': {'id': login_user.pk, 'coins': '895.0'},
                'after_bet': {'id': login_user.pk, 'coins': '1095.0'},
                'won': True
            }
            
        

            response = self.client.post(url, json.dumps(inputs), content_type="application/json")

            self.assertEqual(response.status_code, HTTPStatus.OK)
            self.assertEqual(json.loads(response.content), expected_output)

    def test_fail_post_bets(self):
        with patch.object(Roleta, 'create_default', lambda : Roleta.create_static(1)):
            login_user = create_user()
            user_wallet  = login_user.wallet
            
            user_wallet.total_caiocoins = 1000
            user_wallet.save()

            self.client.force_login(login_user)
            
            url = resolve_url('postbets')

            response = self.client.get(url)

            self.assertRaises(Http404)

    def test_post_coins(self):
        login_user = create_user()
        self.client.force_login(login_user)
        url = resolve_url('postcoins')
        value = 10
        value_response = '10,00'
        obj = {'user_id': login_user.pk, 'value': value}

        response = self.client.post(url, json.dumps(obj), content_type="application/json")
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(json.loads(response.content)['coins'], value_response)

    def test_fail_post_coins(self):
        login_user = create_user()
        self.client.force_login(login_user)
        
        url = resolve_url('postcoins')

        response = self.client.get(url)
        self.assertRaises(Http404)

class AuthWalletViewsTest(TestCase):

    def test_my_wallet(self):
        login_user = create_user()
        self.client.force_login(login_user)
        url = resolve_url('movement')
        response = self.client.get(url)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(list(response.context['object_list']), [])

    def test_my_movement_with_data(self):
        login_user = create_user()
        self.client.force_login(login_user)
        url = resolve_url('movement')
        m1 = Movement.objects.create(
            wallet=login_user.wallet,
            description='m1',
            value=10,
        )
        m2 = Movement.objects.create(
            wallet=login_user.wallet,
            description='m2',
            value=-10,
        )
        response = self.client.get(url)

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(list(response.context['object_list']), [m2, m1])