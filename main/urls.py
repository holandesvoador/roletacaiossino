from django.urls import path
from . import views

urlpatterns = [
    path('mywallet', views.MyWalletView.as_view(), name='wallet'),
    path('movement', views.MovementListView.as_view(), name='movement'),
    path('roleta', views.GameView.as_view(), name='roleta'),
    path('postbets', views.post_bets, name='postbets'),
    path('postcoins', views.post_coins, name='postcoins')
]