from typing import Any
import json
from django.http import HttpRequest
from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views import generic
from .models import Wallet, Movement
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from roleta import Roleta, Dealer
from django.conf import settings


def update_user_movement(user_id, value, movement_description):
    if value == 0:
        return

    wallet = Wallet.objects.filter(user=user_id).first()
    new_movement = Movement(wallet=wallet, 
                            description=movement_description,
                            value=value)
    new_movement.save()

    return True

class MyWalletView(LoginRequiredMixin, generic.DetailView):
    model = Wallet
    template_name = "wallet/wallet.html"

    def get_object(self):
        return super().get_queryset().get(user=self.request.user)

class MovementListView(LoginRequiredMixin, generic.ListView):
    model = Movement
    template_name = "movements/list.html"

    def get_queryset(self):
        return super().get_queryset().filter(wallet__user=self.request.user).order_by('-pk')
    
class GameView(LoginRequiredMixin, generic.TemplateView):
    template_name = "games/roleta.html"

def get_coins_by_ids(ids):
    id_coins = {}

    for id in ids:
        id_coins['id'] = id
        id_coins['coins'] = f"{(Wallet.objects.filter(user=id).first().total_caiocoins):.1f}"

    return id_coins

@csrf_exempt
def post_bets(request: HttpRequest):
    if request.method != "POST":
        raise Http404()
    
    roleta = Roleta.create_default()

    data = json.loads(request.body.decode())
    dealer = Dealer.create_from_inputs(data)

    unique_ids = set(map(lambda x: x['id'], data))

    for input in dealer.inputs:
        update_user_movement(input['id'], -input['value'], 'Bet')

    before_bet = get_coins_by_ids(unique_ids)

    secret_number = roleta.pick()
    computed_bets = dealer.compute_bets(secret_number)
    won = False
    for bet in computed_bets:
        state = update_user_movement(bet['id'], bet['value'], 'Won')
        if state:
            won = True

    after_bet = get_coins_by_ids(unique_ids)

    obj = {
        'secret_number': secret_number,
        'before_bet': before_bet,
        'after_bet': after_bet,
        'won': won
    }

    return JsonResponse(obj)

@csrf_exempt
def post_coins(request: HttpRequest):
    if request.method != "POST":
        raise Http404()
    
    data = json.loads(request.body.decode())

    update_user_movement(data['user_id'], data['value'], 'Add')

    user_coins = Wallet.objects.filter(user=data['user_id']).first().total_caiocoins 
    formatted_coins = f'{user_coins:.2f}'
    formatted_coins = formatted_coins.replace('.', ',')

    obj = {
        'coins': formatted_coins
    }

    return JsonResponse(obj)