coverage==7.4.3
dj-database-url==2.1.0
Django==5.0.2
pillow==10.2.0
python-decouple==3.8
