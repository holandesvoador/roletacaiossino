from typing import Sequence, Dict, Callable, Set, List
from functools import reduce
from operator import add
from dataclasses import dataclass, field
from random import random, randint
import math


def number_to_xy(number):
    col = (number - 1) // 3
    lin = (number - 1) % 3
    return col, lin



DEFAULT_NUMBERS = (
    0,
    32,
    15,
    19,
    4,
    21,
    2,
    25,
    17,
    34,
    6,
    27,
    13,
    36,
    11,
    30,
    8,
    23,
    10,
    5,
    24,
    16,
    33,
    1,
    20,
    14,
    31,
    9,
    22,
    18,
    29,
    7,
    28,
    12,
    35,
    3,
    26,
)

DEFAULT_RED_NUMBERS = tuple(DEFAULT_NUMBERS[1::2])
DEFAULT_BLACK_NUMBERS = tuple(DEFAULT_NUMBERS[2::2])


class InvalidBet(Exception):
    pass


def distance_2d(a, b):
    p = tuple_sub(a, b)
    return (p[0] ** 2 + p[1] ** 2) ** 0.5


def tuple_sub(a, b):
    return tuple(
        a_item - b_item
        for a_item, b_item
        in zip(a, b)
    )

def range_bets():
    yield '1:12'
    yield '13:24'
    yield '25:36'
    yield '1:18'
    yield '19:36'
    yield from [f'{i + 1}:{i + 3}' for i in range(0, 35, 3)]
    yield from [f'{i + 1}:{i + 6}' for i in range(0, 32, 3)]


def range_bets_set():
    return set(range_bets())



@dataclass
class Bet:
    value: float
    numbers: Sequence[int]

    @classmethod
    def create_black(cls, value: float):
        return cls(value=value, numbers=set(DEFAULT_BLACK_NUMBERS))

    @classmethod
    def create_red(cls, value: float):
        return cls(value=value, numbers=set(DEFAULT_RED_NUMBERS))

    @classmethod
    def create_from_filter(cls, value: float, fn):
        return cls(
            value=value,
            numbers=set(filter(fn, DEFAULT_NUMBERS[1:]))
        )

    @classmethod
    def create_odd(cls, value: float):
        return cls.create_from_filter(value, lambda it: it % 2 == 1)

    @classmethod
    def create_even(cls, value: float):
        return cls.create_from_filter(value, lambda it: it % 2 == 0)

    @classmethod
    def create_from_range(cls, value: float, range):
        return cls(value, set(range))
    
    @classmethod
    def create_dozen(cls, value: float, offset: int):
        return cls.create_from_range(value, range(1 + (12 * offset), 12 * (offset+1) + 1))

    @classmethod
    def create_first_dozen(cls, value: float):
        return cls.create_dozen(value, 0)

    @classmethod
    def create_second_dozen(cls, value: float):
        return cls.create_dozen(value, 1)

    @classmethod
    def create_third_dozen(cls, value: float):
        return cls.create_dozen(value, 2)

    @classmethod
    def create_column(cls, value: float, start):
        return cls.create_from_range(value, range(start, 34+start, 3))

    @classmethod
    def create_first_column(cls, value: float):
        return cls.create_column(value, 1)
    
    @classmethod
    def create_second_column(cls, value: float):
        return cls.create_column(value, 2)

    @classmethod
    def create_third_column(cls, value: float):
        return cls.create_column(value, 3)

    @classmethod
    def create_low(cls, value):
        return cls.create_from_range(value, range(1, 19))
    
    @classmethod
    def create_high(cls, value):
        return cls.create_from_range(value, range(19, 37))
    
    def get_win_factor(self):
        return math.floor(35/len(self.numbers))

def custom_bets():
    yield 'black', Bet.create_black
    yield 'red', Bet.create_red
    yield 'odd', Bet.create_odd
    yield 'even', Bet.create_even
    yield 'first_dozen', Bet.create_first_dozen
    yield 'second_dozen', Bet.create_second_dozen
    yield 'third_dozen', Bet.create_third_dozen
    yield 'high', Bet.create_high
    yield 'low', Bet.create_low
    yield 'first_column', Bet.create_first_column
    yield 'second_column', Bet.create_second_column
    yield 'third_column', Bet.create_third_column

def custom_bets_dict():
    return dict(custom_bets())

@dataclass
class BetFactory:

    range_bets: Set[str] = field(default_factory=range_bets_set)
    custom_bets: Dict[str, Callable] = field(default_factory=custom_bets_dict)

    def create_from_range(self, value: float, initial: int, final: int):
        return Bet.create_from_range(value, range(initial, final + 1))

    def create_from_str(self, value: float, raw_str):
        if raw_str in self.custom_bets:
            return self.custom_bets[raw_str](value)

        if raw_str in self.range_bets:
            a, b = raw_str.split(':', 1)
            return self.create_from_range(value, int(a), int(b))
        
        numbers = set(map(int, raw_str.split(',')))
        positions = list(map(number_to_xy, numbers))
        initial_pos = min(positions)
        greater_distance = distance_2d(max(positions, key=lambda it: distance_2d(it, initial_pos)), initial_pos)
        max_distance = 1 if len(numbers) == 2 else 1.5

        if len(numbers) > 4:
            raise InvalidBet()

        if greater_distance > max_distance:
            raise InvalidBet()
        
        return Bet(value=0, numbers=numbers)


@dataclass
class Roleta:
    
    number_strategy: Callable[[], int]

    @classmethod
    def create_static(cls, number: int):
        return cls(
            number_strategy=lambda: number,
        )
    
    @classmethod
    def create_default(cls):
        return cls(
            number_strategy=lambda: randint(0, 36)
        )
    
    def pick(self):
        return self.number_strategy()


@dataclass
class Dealer:

    inputs: List[Dict] 

    @classmethod
    def create_from_inputs(cls, inputs: List[Dict]):
        return cls(
            inputs=inputs
        )

    def compute_bets(self, secret_number: int):
        factory = BetFactory()
        computed_bets = []
        for input in self.inputs:
            computed_bet = {}

            id = input['id']
            value = input['value']
            str = input['str']

            win_factor = 0

            bet = factory.create_from_str(value, str)
            if secret_number in bet.numbers:
                win_factor = bet.get_win_factor()

            computed_bet['id'] = id
            computed_bet['value'] = 0 if win_factor == 0 else (value*win_factor) + value
            computed_bets.append(computed_bet)

        return computed_bets